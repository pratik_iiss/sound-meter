//
//  SoundValueTableViewCell.swift
//  Sound Meter
//
//  Created by Aditya on 23/09/21.
//

import UIKit

class SoundValueTableViewCell: UITableViewCell {
    
    //MARK:- @IBOutlet
    @IBOutlet weak var lblTimeFrame: UILabel!
    @IBOutlet weak var lblOutput: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
