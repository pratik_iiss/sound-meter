//
//  DashBoardViewController.swift
//  Sound Meter
//
//  Created by Aditya on 22/09/21.
//

import UIKit
import AVFoundation

class DashBoardViewController: UIViewController, AVAudioRecorderDelegate {

    //MARK:-  @IBOutlet
    @IBOutlet weak var btnMainScreen: UIButton!
    @IBOutlet weak var viewMeasuringSound: UIView!
    @IBOutlet weak var lblStatusTitle: UILabel!
    @IBOutlet weak var viewDataRecorded: UIView!
    @IBOutlet weak var lblDisplaySecond: UILabel!
    @IBOutlet weak var lblDisplayDBA: UILabel!
    @IBOutlet weak var viewTableView: UIView!
    @IBOutlet weak var lblTimeFrameTitle: UILabel!
    @IBOutlet weak var lblOutPutTitle: UILabel!
    @IBOutlet weak var mainTableview: UITableView!
    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var btnDataStart: UIButton!
    @IBOutlet weak var lctTableConstraintHeight: NSLayoutConstraint!
    
    var audioRecorder: AVAudioRecorder?
    var audioPlayer = AVAudioPlayer()
    var timer: Timer?
    var arrDataRecorded: [Double] = [Double]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
        self.setAudioSession()
    }
    
    func setUpUI() {
        viewMeasuringSound.isHidden = false
        viewDataRecorded.isHidden = true
        viewTableView.isHidden = true
        
        btnStart.layer.cornerRadius = btnStart.frame.height/2
        btnStart.clipsToBounds = true
        
        btnDataStart.layer.cornerRadius = btnDataStart.frame.height/2
        btnDataStart.clipsToBounds = true
        
        btnMainScreen.layer.cornerRadius = btnMainScreen.frame.height/2
        btnMainScreen.clipsToBounds = true

        lblTimeFrameTitle.layer.borderColor = UIColor.black.cgColor
        lblTimeFrameTitle.layer.borderWidth = 1.0
        
        lblOutPutTitle.layer.borderColor = UIColor.black.cgColor
        lblOutPutTitle.layer.borderWidth = 1.0
        
        mainTableview.delegate = self
        mainTableview.dataSource = self
        
        lblStatusTitle.text = "Click Start to measure sound!!"
    }
    
    override var prefersStatusBarHidden: Bool {
      return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
      return .lightContent
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }
    
    func setAudioSession() {
        let audioSession = AVAudioSession.sharedInstance()
   
        do {
            try audioSession.setCategory(.playAndRecord)
        } catch {
        }
        do {
            try audioSession.setActive(true)
        } catch {
        }
    }
    
    func getSavePath() -> URL? {
        let documents = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0])
        let url = documents.appendingPathComponent("record.caf")
        return url
    }
    
    func getAudioSetting() -> [String: Any]? {
        let recordSettings: [String: Any] = [
            AVFormatIDKey:              kAudioFormatAppleIMA4,
            AVSampleRateKey:            44100.0,
            AVNumberOfChannelsKey:      2,
            AVEncoderBitRateKey:        12800,
            AVLinearPCMBitDepthKey:     16,
            AVEncoderAudioQualityKey:   AVAudioQuality.max.rawValue
        ]
        return recordSettings
    }
    
    func getAudioRecorder() -> AVAudioRecorder? {
        if (audioRecorder == nil) {
            let url = getSavePath()
            let setting = getAudioSetting()
            let error: Error? = nil
            do {
                if let url = url, let setting = setting {
                    audioRecorder = try! AVAudioRecorder(url: url, settings: setting)
                    audioRecorder?.delegate = self
                    audioRecorder?.isMeteringEnabled = true
                    
                    if error != nil {
                        print("error：\(error?.localizedDescription ?? "")")
                        return nil
                    }
                }
                
            }
        }
        return audioRecorder!
    }
    
    func setTimer() -> Timer? {
        if (timer == nil) {
            timer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(audioPowerChange), userInfo: nil, repeats: true)
        }
        return timer
    }
    
    @objc func audioPowerChange() {
        getAudioRecorder()?.updateMeters()
        let power = audioRecorder?.averagePower(forChannel: 0)
        let powerMax = audioRecorder?.peakPower(forChannel: 0)
        print("power = \(power), powerMax = \(powerMax)")
       
        let dB = Double(110+CGFloat(power ?? 0.0))
//        let dB = Double(95+CGFloat(power ?? 0.0))
//        power = power! + 160 - 50
//
//        var dB: Double = 0.0
//        if power! < 0.0 {
//            dB = 0
//        } else if power! < 40.0 {
//            dB = Double(power! * 0.875)
//        } else if power! < 100.0 {
//            dB = Double(power! - 15)
//        } else if power! < 110.0 {
//            dB = Double(power! * 2.5 - 165)
//        } else {
//            dB = 110.0
//        }
        self.getRecordData(db: dB)
        print("dB \(dB)")
    }
    
    func getRecordData(db: Double) {
        if arrDataRecorded.count == 0 {
            arrDataRecorded.insert(db,at: 0)
            viewMeasuringSound.isHidden = false
            viewDataRecorded.isHidden = true
            viewTableView.isHidden = false
            mainTableview.reloadData()
            lctTableConstraintHeight.constant =  CGFloat((30 * arrDataRecorded.count))//self.mainTableview.contentSize.height
            return
        } else if arrDataRecorded.count == 1 {
            arrDataRecorded.insert(db, at: 1)
            viewMeasuringSound.isHidden = false
            viewDataRecorded.isHidden = true
            viewTableView.isHidden = false
            mainTableview.reloadData()
            lctTableConstraintHeight.constant =  CGFloat((30 * arrDataRecorded.count))
            return
        } else if arrDataRecorded.count == 2 {
            arrDataRecorded.insert(db, at: 2)
            viewMeasuringSound.isHidden = false
            viewDataRecorded.isHidden = true
            viewTableView.isHidden = false
            mainTableview.reloadData()
            lctTableConstraintHeight.constant =  CGFloat((30 * arrDataRecorded.count))
            return
        } else if arrDataRecorded.count == 3 {
            arrDataRecorded.insert(db, at: 3)
            viewMeasuringSound.isHidden = false
            viewDataRecorded.isHidden = true
            viewTableView.isHidden = false
            mainTableview.reloadData()
            lctTableConstraintHeight.constant =  CGFloat((30 * arrDataRecorded.count))
            return
        } else if arrDataRecorded.count == 4 {
            arrDataRecorded.insert(db, at: 4)
            viewMeasuringSound.isHidden = false
            viewDataRecorded.isHidden = true
            viewTableView.isHidden = false
            mainTableview.reloadData()
            lctTableConstraintHeight.constant =  CGFloat((30 * arrDataRecorded.count))
            return
        } else if arrDataRecorded.count == 5 {
            arrDataRecorded.insert(db, at: 5)
            viewMeasuringSound.isHidden = false
            viewDataRecorded.isHidden = true
            viewTableView.isHidden = false
            mainTableview.reloadData()
            lctTableConstraintHeight.constant =  CGFloat((30 * arrDataRecorded.count))
            return
        } else if arrDataRecorded.count == 6 {
            arrDataRecorded.insert(db, at: 6)
            viewMeasuringSound.isHidden = false
            viewDataRecorded.isHidden = true
            viewTableView.isHidden = false
            mainTableview.reloadData()
            lctTableConstraintHeight.constant =  CGFloat((30 * arrDataRecorded.count))
            return
        } else if arrDataRecorded.count == 7 {
            arrDataRecorded.insert(db, at: 7)
            viewMeasuringSound.isHidden = false
            viewDataRecorded.isHidden = true
            viewTableView.isHidden = false
            mainTableview.reloadData()
            lctTableConstraintHeight.constant =  CGFloat((30 * arrDataRecorded.count))
            return
        } else if arrDataRecorded.count == 8 {
            arrDataRecorded.insert(db, at: 8)
            viewMeasuringSound.isHidden = false
            viewDataRecorded.isHidden = true
            viewTableView.isHidden = false
            mainTableview.reloadData()
            lctTableConstraintHeight.constant =  CGFloat((30 * arrDataRecorded.count))
            return
        } else if arrDataRecorded.count == 9 {
            arrDataRecorded.insert(db, at: 9)
            viewMeasuringSound.isHidden = false
            viewDataRecorded.isHidden = true
            viewTableView.isHidden = false
            mainTableview.reloadData()
            lctTableConstraintHeight.constant =  CGFloat((30 * arrDataRecorded.count))
            return
        } else if arrDataRecorded.count == 10 {
            arrDataRecorded.insert(db,at: 10)
            viewMeasuringSound.isHidden = false
            viewDataRecorded.isHidden = true
            viewTableView.isHidden = false
            mainTableview.reloadData()
            mainTableview.layoutIfNeeded()
            lctTableConstraintHeight.constant =  CGFloat((30 * arrDataRecorded.count))
            return
        }  else if arrDataRecorded.count == 11 {
            self.viewMeasuringSound.isHidden = true
            self.viewTableView.isHidden = false
            self.viewDataRecorded.isHidden = false
            self.getSortData()
            self.getAudioRecorder()!.stop()
            self.setTimer()?.fireDate = .distantFuture
        }
    }
    
    func getSortData() {
        let sorted = arrDataRecorded.sorted(by: {$0 > $1})
        let lastSort = sorted[0]
        print(lastSort)
        
        if lastSort == arrDataRecorded[0] {
            lblDisplaySecond.text = "0.2 second"
            lblDisplayDBA.text = "\(String(format: "%.1f", lastSort)) dBA"
        } else if lastSort == arrDataRecorded[1] {
            lblDisplaySecond.text = "0.4 second"
            lblDisplayDBA.text = "\(String(format: "%.1f", lastSort)) dBA"
        } else if lastSort == arrDataRecorded[2] {
            lblDisplaySecond.text = "0.6 second"
            lblDisplayDBA.text = "\(String(format: "%.1f", lastSort)) dBA"
        } else if lastSort == arrDataRecorded[3] {
            lblDisplaySecond.text = "0.8 second"
            lblDisplayDBA.text = "\(String(format: "%.1f", lastSort)) dBA"
        } else if lastSort == arrDataRecorded[4] {
            lblDisplaySecond.text = "1.0 second"
            lblDisplayDBA.text = "\(String(format: "%.1f", lastSort)) dBA"
        } else if lastSort == arrDataRecorded[5] {
            lblDisplaySecond.text = "1.2 second"
            lblDisplayDBA.text = "\(String(format: "%.1f", lastSort)) dBA"
        } else if lastSort == arrDataRecorded[6] {
            lblDisplaySecond.text = "1.4 second"
            lblDisplayDBA.text = "\(String(format: "%.1f", lastSort)) dBA"
        } else if lastSort == arrDataRecorded[7] {
            lblDisplaySecond.text = "1.6 second"
            lblDisplayDBA.text = "\(String(format: "%.1f", lastSort)) dBA"
        } else if lastSort == arrDataRecorded[8] {
            lblDisplaySecond.text = "1.8 second"
            lblDisplayDBA.text = "\(String(format: "%.1f", lastSort)) dBA"
        } else if lastSort == arrDataRecorded[9] {
            lblDisplaySecond.text = "2.0 second"
            lblDisplayDBA.text = "\(String(format: "%.1f", lastSort)) dBA"
        }
    }
    
    //MARK:- Button Action.
    @IBAction func btnMainScreenClicked(_ sender: UIButton) {
        let demoVc = self.storyboard?.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        self.navigationController?.pushViewController(demoVc, animated: true)
    }
    
    @IBAction func btnStartClicked(_ sender: UIButton) {
        self.btnStart.isUserInteractionEnabled = true
        if !getAudioRecorder()!.isRecording {
            lblStatusTitle.text = "Measuring Sound..."
            self.btnStart.isUserInteractionEnabled = false
            arrDataRecorded.removeAll()
            viewMeasuringSound.isHidden = false
            viewDataRecorded.isHidden = true
            viewTableView.isHidden = true
            getAudioRecorder()!.record()
            setTimer()?.fireDate = .distantPast
        }
    }
    
    @IBAction func btnStartDataClicked(_ sender: UIButton) {
        if !getAudioRecorder()!.isRecording {
            arrDataRecorded.removeAll()
            viewMeasuringSound.isHidden = false
            viewDataRecorded.isHidden = true
            viewTableView.isHidden = true
            getAudioRecorder()!.record()
            setTimer()?.fireDate = .distantPast
        }
    }
}

// MARK: - UITableView Delegate Methods
extension DashBoardViewController: UITableViewDelegate,UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.arrDataRecorded.count
    }
  
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        
        tableView.register(UINib(nibName: "SoundValueTableViewCell", bundle: nil), forCellReuseIdentifier: "cell")
        
        //========== TABLEVIEW CELL PROGRAMMATICALLY ==========//
        let cell:SoundValueTableViewCell! = tableView.dequeueReusableCell(withIdentifier: "cell") as? SoundValueTableViewCell
        let model = self.arrDataRecorded[indexPath.row]
        
        if indexPath.row == 0 {
            cell.lblTimeFrame.text = "0.2"
            cell.lblOutput.text = "\(String(format: "%.1f", model))"
        } else if indexPath.row == 1 {
            cell.lblTimeFrame.text = "0.4"
            cell.lblOutput.text = "\(String(format: "%.1f", model))"
        } else if indexPath.row == 2 {
            cell.lblTimeFrame.text = "0.6"
            cell.lblOutput.text = "\(String(format: "%.1f", model))"
        } else if indexPath.row == 3 {
            cell.lblTimeFrame.text = "0.8"
            cell.lblOutput.text = "\(String(format: "%.1f", model))"
        } else if indexPath.row == 4 {
            cell.lblTimeFrame.text = "1.0"
            cell.lblOutput.text = "\(String(format: "%.1f", model))"
        } else if indexPath.row == 5 {
            cell.lblTimeFrame.text = "1.2"
            cell.lblOutput.text = "\(String(format: "%.1f", model))"
        } else if indexPath.row == 6 {
            cell.lblTimeFrame.text = "1.4"
            cell.lblOutput.text = "\(String(format: "%.1f", model))"
        } else if indexPath.row == 7 {
            cell.lblTimeFrame.text = "1.6"
            cell.lblOutput.text = "\(String(format: "%.1f", model))"
        } else if indexPath.row == 8 {
            cell.lblTimeFrame.text = "1.8"
            cell.lblOutput.text = "\(String(format: "%.1f", model))"
        } else if indexPath.row == 9 {
            cell.lblTimeFrame.text = "2.0"
            cell.lblOutput.text = "\(String(format: "%.1f", model))"
        }
        return cell
        
    }
   
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
    }
}

