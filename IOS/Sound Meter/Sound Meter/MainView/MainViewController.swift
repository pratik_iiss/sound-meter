//
//  MainViewController.swift
//  Sound Meter
//
//  Created by Aditya on 29/09/21.
//

import Foundation
import UIKit
import AVFoundation
import CoreAudio
import CoreData
import GaugeSlider

class MainViewController: UIViewController,AVAudioRecorderDelegate {
    
    @IBOutlet weak var dbTrackerView: GaugeSliderView!
    @IBOutlet weak var minimumLevelLabel: UILabel!
    @IBOutlet weak var maximumLevelLabel: UILabel!
    @IBOutlet weak var averageLevelLabel: UILabel!
    @IBOutlet weak var btnReset: UIButton!
    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var btnStop: UIButton!
    @IBOutlet weak var btnBack: UIButton!
    
    var managedObjextContext: NSManagedObjectContext!
    var recorder: AVAudioRecorder!
    var levelTimer = Timer()
    var arrayLevel: [CGFloat]! = []
    let LEVEL_THRESHOLD: Float = -40.0
    var minimumLevel = CGFloat()
    var maximumLevel = CGFloat()
    var averageSoundLevel = Int()
    var audioEngine: AVAudioEngine = AVAudioEngine()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnStart.layer.cornerRadius = btnStart.frame.height/2
        btnStart.clipsToBounds = true
        
        btnStop.layer.cornerRadius = btnStop.frame.height/2
        btnStop.clipsToBounds = true
        
        managedObjextContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
        
        dbTrackerView.onButtonAction = { [weak self] in
            self!.addRecord()
        }
        
        let documents = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0])
        let url = documents.appendingPathComponent("record.caf")
        
        let recordSettings: [String: Any] = [
            AVFormatIDKey:              kAudioFormatAppleIMA4,
            AVSampleRateKey:            44100.0,
            AVNumberOfChannelsKey:      2,
            AVEncoderBitRateKey:        12800,
            AVLinearPCMBitDepthKey:     16,
            AVEncoderAudioQualityKey:   AVAudioQuality.max.rawValue
        ]
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.playAndRecord)
            try audioSession.setActive(true)
            try recorder = AVAudioRecorder(url:url, settings: recordSettings)
            
        } catch {
            return
        }
        
        recorder.prepareToRecord()
        recorder.isMeteringEnabled = true
        recorder.record()
        self.btnStart.isUserInteractionEnabled = false
        
        levelTimer = Timer.scheduledTimer(timeInterval: 0.2, target: self, selector: #selector(levelTimerCallback), userInfo: nil, repeats: true)
        
        setupUserInterface()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = true
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
    }
    
    override var prefersStatusBarHidden: Bool {
      return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
      return .lightContent
    }
    
    @objc func levelTimerCallback() {
        recorder.updateMeters()
        
        let level = recorder.averagePower(forChannel: 0)
        dbTrackerView.value = CGFloat(110+(level))//CGFloat(db)//= CGFloat(95+level)
        
        let detectStatus = dbTrackerView.value
        
        if detectStatus <= 70 {
            dbTrackerView.fillPathColor = .systemGreen
            //dbResultLabel.text = "Normal conversation"
//            dbTrackerView.placeholder = "Normal conversation"
        } else if detectStatus >= 70 && detectStatus <= 85 {
            dbTrackerView.fillPathColor = .systemOrange
            //dbResultLabel.text = "City traffic (inside the car)"
//            dbTrackerView.placeholder = "City traffic"
        } else if detectStatus >= 85 {
            dbTrackerView.fillPathColor = .systemRed
            //dbResultLabel.text = "Gas-powered lawnmowers\nleaf blowers"
            //dbTrackerView.placeholder = "Gas-powered lawnmowers"
        }
        
        if dbTrackerView.value > 0 {
            arrayLevel.append(dbTrackerView.value)
            
            minimumLevel = arrayLevel.min()!
            maximumLevel = arrayLevel.max()!
            
            minimumLevelLabel.text = "\(minimumLevel)"
            maximumLevelLabel.text = "\(maximumLevel)"
            
            averageLevel()
        }
    }
    
    func averageLevel(){
        let level:[CGFloat] = arrayLevel
        let levelAvg = level.reduce(0, +) / CGFloat(level.count)
        averageLevelLabel.text = String(format: "%.1f", levelAvg)
        averageSoundLevel = Int(String(format: "%.1f", levelAvg)) ?? 0
    }
    
    func addRecord() {
        let now = Date()
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "dd, MMMM"
        let dateString = formatter.string(from: now)
        
        let dbItem = SoundData(context: self.managedObjextContext)
        dbItem.maximum = Int16(self.maximumLevel)
        dbItem.minimum = Int16(self.minimumLevel)
        dbItem.average = Int16(self.averageSoundLevel)
        dbItem.currentDate = dateString
        
        do {
            try self.managedObjextContext.save()
        } catch {
            print("Could not save data \(error.localizedDescription)")
        }
    }

    @IBAction func btnBackClicked(_ sender: UIButton) {
        recorder!.stop()
        levelTimer.fireDate = .distantFuture
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func btnResetClicked(_ sender: UIButton) {
        if recorder!.isRecording {
            recorder!.stop()
            dbTrackerView.value = CGFloat(0.0)
            averageSoundLevel = Int(0)
            maximumLevelLabel.text = "-"
            arrayLevel.removeAll()
            levelTimer.fireDate = .distantFuture
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                if !self.recorder!.isRecording {
                    self.btnStart.isUserInteractionEnabled = false
                    self.btnStop.isUserInteractionEnabled = true
                    self.btnReset.isUserInteractionEnabled = true
                    self.recorder.record()
                    self.levelTimer.fireDate = .distantPast
                }
            }
        }
    }
    
    @IBAction func btnStartClicked(_ sender: UIButton) {
        if !recorder!.isRecording {
            self.btnStart.isUserInteractionEnabled = false
            self.btnStop.isUserInteractionEnabled = true
            self.btnReset.isUserInteractionEnabled = true
            recorder.record()
            levelTimer.fireDate = .distantPast
        }
    }
    
    @IBAction func btnStopClicked(_ sender: UIButton) {
        recorder!.stop()
        self.btnStart.isUserInteractionEnabled = true
        self.btnStop.isUserInteractionEnabled = false
        self.btnReset.isUserInteractionEnabled = false
        dbTrackerView.value = CGFloat(0.0)
        averageSoundLevel = Int(0)
        maximumLevelLabel.text = "-"
        levelTimer.fireDate = .distantFuture
    }
    
    func setupUserInterface() {
        view.addVerticalGradientLayer(topColor: primaryColor, bottomColor: secondaryColor)
        
        dbTrackerView.unit = " dBA"
        dbTrackerView.placeholderColor = .white
        dbTrackerView.placeholderFont = UIFont.systemFont(ofSize: 15, weight: .regular)
        dbTrackerView.unitColor = UIColor(hex: "FFFFFF")
        dbTrackerView.blankPathColor = UIColor(hex: "FFFFFF")
        dbTrackerView.indicatorColor = UIColor(red: 94/255, green: 187/255, blue: 169/255, alpha: 1)
        dbTrackerView.customControlButtonVisible = false
        dbTrackerView.unitIndicatorFont = UIFont.systemFont(ofSize: 0.1, weight: .medium)
        dbTrackerView.unitFont = UIFont.systemFont(ofSize: 50)
        
        if #available(iOS 13.0, *) {
            overrideUserInterfaceStyle = .light
        }
    }
}
