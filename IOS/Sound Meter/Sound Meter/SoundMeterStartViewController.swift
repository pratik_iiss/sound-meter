//
//  SoundMeterStartViewController.swift
//  Sound Meter
//
//  Created by Aditya on 23/09/21.
//

import UIKit

class SoundMeterStartViewController: UIViewController {
    
    //MARK:- @IBOutlet
    @IBOutlet weak var lblTitle: UILabel!
    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var btnDemo: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        btnStart.layer.cornerRadius = 8
        btnStart.clipsToBounds = true
        
        btnDemo.layer.cornerRadius = 8
        btnDemo.clipsToBounds = true
    }
    
    override var prefersStatusBarHidden: Bool {
      return false
    }
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
      return .lightContent
    } 
    
    //MARK:- Button Action
    @IBAction func btnStartClicked(_ sender: UIButton) {
        let dashBoardVc = self.storyboard?.instantiateViewController(withIdentifier: "DashBoardViewController") as! DashBoardViewController
        self.navigationController?.pushViewController(dashBoardVc, animated: true)
    }
    
    @IBAction func btnDemo(_ sender: UIButton) {
        let demoVc = self.storyboard?.instantiateViewController(withIdentifier: "SoundMeterDemoViewController") as! SoundMeterDemoViewController
        self.navigationController?.pushViewController(demoVc, animated: true)
    }
}
