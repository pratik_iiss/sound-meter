//
//  UITabBar.swift
//  Sound Meter
//
//  Created by Aditya on 29/09/21.
//

import UIKit

extension UITabBar {
    
    static func setTransparentTabbar() {
        UITabBar.appearance().backgroundImage = UIImage()
        UITabBar.appearance().shadowImage     = UIImage()
        UITabBar.appearance().clipsToBounds   = true
    }
}
