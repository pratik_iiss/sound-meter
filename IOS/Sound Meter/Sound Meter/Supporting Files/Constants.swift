//
//  Constants.swift
//  Sound Meter
//
//  Created by Aditya on 29/09/21.
//

import Foundation
import UIKit

//---IMPORTANT CONSTANTS (DO NOT CHANGE)---//

var primaryColor = UIColor(hex: "1A1B46")
var secondaryColor = UIColor(hex: "111111")

var dbDataValues = [SoundData]()

//---IMPORTANT CONSTANTS (DO NOT CHANGE)---//
