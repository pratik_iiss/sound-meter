//
//  SoundMeterDemoViewController.swift
//  Sound Meter
//
//  Created by Aditya on 24/09/21.
//

import UIKit
import AVFoundation

class SoundMeterDemoViewController: UIViewController, AVAudioRecorderDelegate {
    
    //MARK:-  @IBOutlet
    @IBOutlet weak var viewSoundValue: UIView!
    @IBOutlet weak var lblSoundValue: UILabel!
    @IBOutlet weak var btnStart: UIButton!
    @IBOutlet weak var btnStop: UIButton!
    
    var recorder: AVAudioRecorder!
    var levelTimer = Timer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpUI()
    }
    
    func setUpUI() {
        self.title = "Live Tracking"
        
        let backBarButtonItem = UIBarButtonItem(title: "Back", style: .done, target: self, action: #selector(backLiveTracking))
        backBarButtonItem.tintColor = .white
        self.navigationItem.leftBarButtonItem  = backBarButtonItem
        
        viewSoundValue.layer.cornerRadius = 10
        viewSoundValue.clipsToBounds = true
        
        btnStart.layer.cornerRadius = 8
        btnStart.clipsToBounds = true
        
        btnStop.layer.cornerRadius = 8
        btnStop.clipsToBounds = true
        
        let documents = URL(fileURLWithPath: NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)[0])
        let url = documents.appendingPathComponent("record.caf")
        
        let recordSettings: [String: Any] = [
            AVFormatIDKey:              kAudioFormatLinearPCM,
            AVSampleRateKey:            44100.0,
            AVNumberOfChannelsKey:      2,
            AVEncoderBitRateKey:        12800,
            AVLinearPCMBitDepthKey:     16,
            AVEncoderAudioQualityKey:   AVAudioQuality.max.rawValue
        ]
        
        let audioSession = AVAudioSession.sharedInstance()
        do {
            try audioSession.setCategory(AVAudioSession.Category.playAndRecord)
            try audioSession.setActive(true)
            try recorder = AVAudioRecorder(url:url, settings: recordSettings)
            
        } catch {
            return
        }
        
        recorder.prepareToRecord()
        recorder.isMeteringEnabled = true
        recorder.record()
        
        levelTimer = Timer.scheduledTimer(timeInterval: 0.1, target: self, selector: #selector(levelTimerCallback), userInfo: nil, repeats: true)
    }
    
    @objc func levelTimerCallback() {
        recorder.updateMeters()
        
        let level = recorder.averagePower(forChannel: 0)
        
        let dB = Double(95+CGFloat(level))
        
        lblSoundValue.text = "\(String(format: "%.1f", dB)) dBA"
        print("dB \(dB)")
    }
    
    @objc func backLiveTracking(){
        recorder!.stop()
        levelTimer.fireDate = .distantFuture
        self.navigationController?.popViewController(animated: true)
    }

    @IBAction func btnStartClicked(_ sender: UIButton) {
        if !recorder!.isRecording {
            recorder.record()
            levelTimer.fireDate = .distantPast
        }
    }
    
    @IBAction func btnStopClicked(_ sender: UIButton) {
        recorder!.stop()
        levelTimer.fireDate = .distantFuture
    }
}
