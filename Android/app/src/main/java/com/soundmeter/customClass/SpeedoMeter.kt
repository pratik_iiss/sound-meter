package com.soundmeter.customClass

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import androidx.appcompat.widget.AppCompatImageView
import com.soundmeter.R

class SpeedoMeter: AppCompatImageView {

    val ANIMATION_INTERVAL: Long = 20
    private var ctx: Context? = null
    private var indicatorBitmap: Bitmap? = null
    private val mMatrix = Matrix()
    private var newHeight = 0
    private var newWidth = 0
    private val paint = Paint()
    private var scaleHeight = 0f
    private var scaleWidth = 0f

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)

    private fun getAngle(f: Float): Float {
        return f * 3 / 2
    }

    private fun init() {
        val decodeResource = BitmapFactory.decodeResource(resources, R.drawable.ic_arrow)
        val width = decodeResource.width
        val height = decodeResource.height
        newWidth = getWidth()
        newHeight = getHeight()
        scaleWidth = newWidth.toFloat() / width.toFloat()
        scaleHeight = newHeight.toFloat() / height.toFloat()
        mMatrix.postScale(scaleWidth, scaleHeight)
        indicatorBitmap = Bitmap.createBitmap(decodeResource, 0, 0, width, height, mMatrix, true)
    }

    fun refresh() {
        postInvalidateDelayed(ANIMATION_INTERVAL)
    }

    override fun onDraw(canvas: Canvas) {
        super.onDraw(canvas)
        if (indicatorBitmap == null) {
            init()
        }
        var f: Float = World.dbCount
        if (f <= 0.0f) {
            f = 0.0f
        }
        mMatrix.setRotate(getAngle(f), (newWidth / 2).toFloat(), newHeight.toFloat())
        canvas.drawBitmap(indicatorBitmap!!, mMatrix, paint)
    }
}