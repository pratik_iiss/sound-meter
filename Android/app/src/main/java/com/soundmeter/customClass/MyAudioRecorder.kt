package com.soundmeter.customClass

import android.media.MediaRecorder
import android.os.Build
import android.util.Log
import java.io.File
import java.io.IOException

object MyAudioRecorder {

    val TAG = "MyAudioRecorder"
    private var myRecAudioFile: File = File("")
    lateinit var mMediaRecorder: MediaRecorder
    var isRecording = false

    fun getMaxAmplitude(): Double {
        return try {
            mMediaRecorder.maxAmplitude.toDouble()
        } catch (e: IllegalArgumentException) {
            Log.e(TAG, "getMaxAmplitude: $e")
            return 0.0
        }
    }

    fun getMyRecAudioFile(): File {
        return myRecAudioFile
    }

    fun setMyRecAudioFile(myRecAudioFile: File) {
        this.myRecAudioFile = myRecAudioFile
    }

    fun startRecorder(): Boolean {
        try {
            mMediaRecorder = MediaRecorder()
            mMediaRecorder.setAudioSource(MediaRecorder.AudioSource.MIC)
            mMediaRecorder.setAudioSamplingRate(44100);
            mMediaRecorder.setAudioEncodingBitRate(96000);
            mMediaRecorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP)
            mMediaRecorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB)
            mMediaRecorder.setOutputFile(myRecAudioFile.absolutePath)
            mMediaRecorder.prepare()
            mMediaRecorder.start()
            isRecording = true
            Log.e(TAG, "recorder started")
            return true
        } catch (e: IOException) {
            mMediaRecorder.reset()
            mMediaRecorder.release()
            mMediaRecorder = MediaRecorder()
            isRecording = false
            Log.e(TAG, "startRecorder: $e")
        } catch (e: IllegalStateException) {
            stopRecording()
            Log.e(TAG, "startRecorder: $e")
            isRecording = false
        }
        return false
    }

    fun stopRecording() {
        if (isRecording) {
            try {
                mMediaRecorder.stop()
                mMediaRecorder.release()
            } catch (e: Exception) {
                Log.e(TAG, "stopRecording: $e")
            }
        }
        mMediaRecorder = MediaRecorder()
        isRecording = false
    }

    fun delete() {
        stopRecording()
        myRecAudioFile.delete()
        myRecAudioFile = File("")
    }
}