package com.soundmeter.customClass

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import java.lang.String
import java.util.*

class MyDataSqlite : SQLiteOpenHelper {

    private val DATA_NAME = "Sound_Meter"
    private val VERSION = 1

    private val COLUMN_METER_ID = "Meter_Id"
    private val COLUMN_METER_DATA = "Meter_Data"
    private val COLUMN_METER_TIME = "Meter_Time"
    private val COLUMN_METER_RUN_TIME = "Meter_Run_Time"
    private val COLUMN_METER_TITLE = "Meter_Title"
    private val COLUMN_METER_MIN = "Meter_Min"
    private val COLUMN_METER_AVG = "Meter_AVG"
    private val COLUMN_METER_MAX = "Meter_Max"

    constructor(
        context: Context?,
        name: kotlin.String?,
        factory: SQLiteDatabase.CursorFactory?,
        version: Int
    ) : super(context, name, factory, version)

    override fun onCreate(db: SQLiteDatabase) {
        Log.d("##A", "Create Database")
        val createData =
            ("CREATE TABLE " + DATA_NAME + " ( " + COLUMN_METER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + COLUMN_METER_DATA + " TEXT ," + COLUMN_METER_TIME + " TEXT , " + COLUMN_METER_RUN_TIME + " TEXT , "
                    + COLUMN_METER_TITLE + " TEXT , " + COLUMN_METER_MIN + " TEXT , " + COLUMN_METER_AVG + " TEXT , "
                    + COLUMN_METER_MAX + " TEXT " + " ) ")
        db.execSQL(createData)
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS $DATA_NAME")
        onCreate(db)
    }

    fun getAllMeter(): ArrayList<Meter>? {
        val meter: ArrayList<Meter> = ArrayList<Meter>()
        val selectQuery = "SELECT  * FROM $DATA_NAME"
        val db: SQLiteDatabase = this.writableDatabase
        val cursor = db.rawQuery(selectQuery, null)
        if (cursor.moveToFirst()) {
            do {
                val mMeter = Meter
                mMeter.iID = cursor.getString(0).toString()
                mMeter.sDate = cursor.getString(1)
                mMeter.sTime = cursor.getString(2)
                mMeter.sTimeMeter = cursor.getString(3)
                mMeter.sNotifiMeter = cursor.getString(4)
                mMeter.iMin = cursor.getString(5).toFloat()
                mMeter.iAVG = cursor.getString(6).toFloat()
                mMeter.iMax = cursor.getString(7).toFloat()
                meter.add(mMeter)
            } while (cursor.moveToNext())
        }
        return meter
    }

    fun addNote(meter: Meter): Boolean {
        val db: SQLiteDatabase = this.writableDatabase
        val values = ContentValues()
        values.put(COLUMN_METER_DATA, meter.sDate)
        values.put(COLUMN_METER_TIME, meter.sTime)
        values.put(COLUMN_METER_RUN_TIME, meter.sTimeMeter)
        values.put(COLUMN_METER_TITLE, meter.sNotifiMeter)
        values.put(COLUMN_METER_MIN, meter.iMin)
        values.put(COLUMN_METER_AVG, meter.iAVG)
        values.put(COLUMN_METER_MAX, meter.iMax)
        val kt = db.insert(DATA_NAME, null, values)
        return kt != 0L
    }

    fun deleteMeter(meter: Meter) {
        val db: SQLiteDatabase = this.writableDatabase
        db.delete(DATA_NAME, "$COLUMN_METER_ID = ?", arrayOf(String.valueOf(meter.iID)))
        db.close()
    }

    fun open(): SQLiteDatabase? {
        return this.writableDatabase
    }
}