package com.soundmeter.customClass

object World {

    var origin = 0.0f
    var dbCount = 0.0f
    var lastDbCount = dbCount
    var lastorigin = origin
    var maxDB = 0.0f
    private val min = 0.5f
    var minDB = 100.0f

    private var value = 0.0f


    fun setDbCount(f: Float, f2: Float) {
        var f = f
        var f3: Float
        var f4 = lastDbCount
        val f5: Float
        if (f > f4) {
            f5 = f - f4
            f3 = min
            if (f5 > f3) {
                f3 = f - f4
            }
            value = f3
        } else {
            f5 = f - f4
            f3 = min
            value = if (f5 < -f3) f - f4 else -f3
        }
        dbCount = lastDbCount + value * 0.1f
        lastDbCount = dbCount
        f = lastorigin
        if (f2 > f) {
            f4 = f2 - f
            f3 = min
            if (f4 > f3) {
                f3 = f2 - f
            }
            value = f3
        } else {
            f4 = f2 - f
            f3 = min
            value = if (f4 < -f3) f2 - f else -f3
        }
        origin = lastorigin + value * 0.1f
        lastorigin = origin
        f = dbCount
        if (f < minDB) {
            minDB = f
        }
        f = dbCount
        if (f > maxDB) {
            maxDB = f
        }
        if (minDB < 0.0f) {
            minDB = 0.0f
        }
        if (maxDB < 0.0f) {
            maxDB = 0.0f
        }
    }
}