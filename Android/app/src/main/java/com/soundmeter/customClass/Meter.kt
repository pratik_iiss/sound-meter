package com.soundmeter.customClass

object Meter{

    var iID: String? = null
    var sDate: String? = null
    var sTime: String? = null
    var sTimeMeter: String? = null
    var sNotifiMeter: String? = null
    var iMin = 0f
    var iAVG = 0f
    var iMax = 0f


    fun Meter(
        sDate: String?,
        sTime: String?,
        sTimeMeter: String?,
        sNotifiMeter: String?,
        iMin: Float,
        iAVG: Float,
        iMax: Float
    ) {
        this.sDate = sDate
        this.sTime = sTime
        this.sTimeMeter = sTimeMeter
        this.sNotifiMeter = sNotifiMeter
        this.iMin = iMin
        this.iAVG = iAVG
        this.iMax = iMax
    }

    fun Meter() {}
}
