package com.soundmeter.viewdecibel;

import java.util.Random;

public class World {

    static String TAG = "World";
    public static float origin = 0.0f;
    public static float dbCount = 0.0f;
    public static float lastDbCount = dbCount;
    public static float lastorigin = origin;
    public static float maxDB = 0.0f;
    private static final float min = 0.5f;
    public static float minDB = 100.0f;

    private static float value = 0.0f;

    public static void setDbCount(float f, float f2) {
//        Log.e(TAG, "f: " + f + "  f2: " + f2);
        float f3;
        float f4 = lastDbCount;
        float f5;
        if (f > f4) {
            f5 = f - f4;
            f3 = min;
            if (f5 > f3) {
                f3 = f - f4;
            }
            value = f3;
        } else {
            f5 = f - f4;
            f3 = min;
            value = f5 < (-f3) ? f - f4 : -f3;
        }
//        if (f2 > 75)
            dbCount = f2;
//        else
//            dbCount = lastDbCount + (value * 0.1f);

        lastDbCount = dbCount;
        f = lastorigin;
        if (f2 > f) {
            f4 = f2 - f;
            f3 = min;
            if (f4 > f3) {
                f3 = f2 - f;
            }
            value = f3;
        } else {
            f4 = f2 - f;
            f3 = min;
            value = f4 < (-f3) ? f2 - f : -f3;
        }
        origin = lastorigin + (value * 0.1f);
        lastorigin = origin;
        f = dbCount;
        if (f < minDB) {
            minDB = f;
        }
        f = dbCount;
        if (f > maxDB) {
            maxDB = f;
        }
        if (minDB < 0.0f) {
            minDB = 0.0f;
        }
        if (maxDB < 0.0f) {
            maxDB = 0.0f;
        }
    }

    public static void setDbCount2(float log10, float f2) {
        Random random = new Random();
        if (log10 >= 90.0) {
            double B = Double.valueOf((lastDbCount % 5) * 5);
            Double.isNaN(B);
            double d = log10 + B;
            double nextInt = random.nextInt(6);
            Double.isNaN(nextInt);
            dbCount = (float) (d + nextInt);
        }
        if (log10 <= 120.0d && log10 > 0.0d) {
            dbCount = log10;
            double d = lastorigin + (value * 0.1f);
            lastorigin = (float) d;
            double d3 = lastDbCount + log10;
            lastorigin = (float) d3;

        }
    }
}
