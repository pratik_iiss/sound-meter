package com.soundmeter.viewdecibel;

public class Meter {
    public String iID;
    public String sDate;
    public String sTime;
    public String sTimeMeter;
    public String sNotifiMeter;
    public float iMin;
    public float iAVG;
    public float iMax;


    public Meter(String sDate, String sTime, String sTimeMeter, String sNotifiMeter, float iMin, float iAVG, float iMax) {
        this.sDate = sDate;
        this.sTime = sTime;
        this.sTimeMeter = sTimeMeter;
        this.sNotifiMeter = sNotifiMeter;
        this.iMin = iMin;
        this.iAVG = iAVG;
        this.iMax = iMax;
    }

    public Meter() {

    }
}
