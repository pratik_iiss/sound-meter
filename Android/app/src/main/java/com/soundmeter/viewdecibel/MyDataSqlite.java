package com.soundmeter.viewdecibel;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

public class MyDataSqlite extends SQLiteOpenHelper {

    private static final String DATA_NAME = "Sound_Meter";
    private static final int VERSION = 1;

    private static final String COLUMN_METER_ID = "Meter_Id";
    private static final String COLUMN_METER_DATA = "Meter_Data";
    private static final String COLUMN_METER_TIME = "Meter_Time";
    private static final String COLUMN_METER_RUN_TIME = "Meter_Run_Time";
    private static final String COLUMN_METER_TITLE = "Meter_Title";
    private static final String COLUMN_METER_MIN = "Meter_Min";
    private static final String COLUMN_METER_AVG = "Meter_AVG";
    private static final String COLUMN_METER_MAX = "Meter_Max";

    public MyDataSqlite(Context context) {
        super(context, DATA_NAME, null, VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        Log.d("##A", "Create Database");
        String createData = "CREATE TABLE " + DATA_NAME + " ( " + COLUMN_METER_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + COLUMN_METER_DATA + " TEXT ," + COLUMN_METER_TIME + " TEXT , " + COLUMN_METER_RUN_TIME + " TEXT , "
                + COLUMN_METER_TITLE + " TEXT , " + COLUMN_METER_MIN + " TEXT , " + COLUMN_METER_AVG + " TEXT , "
                + COLUMN_METER_MAX + " TEXT " + " ) ";
        db.execSQL(createData);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + DATA_NAME);
        onCreate(db);
    }

    public ArrayList<Meter> getAllMeter() {
        ArrayList<Meter> meter = new ArrayList<Meter>();
        String selectQuery = "SELECT  * FROM " + DATA_NAME;
        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        if (cursor.moveToFirst()) {
            do {
                Meter mMeter = new Meter();
                mMeter.iID = String.valueOf(cursor.getString(0));
                mMeter.sDate = cursor.getString(1);
                mMeter.sTime = cursor.getString(2);
                mMeter.sTimeMeter = cursor.getString(3);
                mMeter.sNotifiMeter = cursor.getString(4);
                mMeter.iMin = Float.parseFloat(cursor.getString(5));
                mMeter.iAVG = Float.parseFloat(cursor.getString(6));
                mMeter.iMax = Float.parseFloat(cursor.getString(7));
                meter.add(mMeter);
            } while (cursor.moveToNext());
        }
        return meter;
    }

    public boolean addNote(Meter meter) {
        SQLiteDatabase db = this.getWritableDatabase();
        ContentValues values = new ContentValues();


        values.put(COLUMN_METER_DATA, meter.sDate);
        values.put(COLUMN_METER_TIME, meter.sTime);
        values.put(COLUMN_METER_RUN_TIME, meter.sTimeMeter);
        values.put(COLUMN_METER_TITLE, meter.sNotifiMeter);
        values.put(COLUMN_METER_MIN, meter.iMin);
        values.put(COLUMN_METER_AVG, meter.iAVG);
        values.put(COLUMN_METER_MAX, meter.iMax);
        long kt = db.insert(DATA_NAME, null, values);
        if (kt != 0) {
            return true;
        }
        return false;
    }

    public void deleteMeter(Meter meter) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(DATA_NAME, COLUMN_METER_ID + " = ?",
                new String[]{String.valueOf(meter.iID)});
        db.close();
    }

    public SQLiteDatabase open() {
        return this.getWritableDatabase();
    }
}
