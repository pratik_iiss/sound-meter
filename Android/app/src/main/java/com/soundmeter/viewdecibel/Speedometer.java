package com.soundmeter.viewdecibel;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.util.AttributeSet;

import androidx.appcompat.widget.AppCompatImageView;

import com.soundmeter.R;
import com.soundmeter.activities.SoundRecorderJava;

public class Speedometer extends AppCompatImageView {
    static final long ANIMATION_INTERVAL = 20;
    private Context ctx;
    private Bitmap indicatorBitmap;
    private Matrix mMatrix = new Matrix();
    private int newHeight;
    private int newWidth;
    private Paint paint = new Paint();
    private float scaleHeight;
    private float scaleWidth;

    private float getAngle(float f) {
        return (f) * 3 / 2;
    }

    public Speedometer(Context context) {
        super(context);
        this.ctx = context;
    }

    public Speedometer(Context context, AttributeSet attributeSet) {
        super(context, attributeSet);
        this.ctx = context;
    }

    private void init() {
        Bitmap decodeResource = BitmapFactory.decodeResource(getResources(), R.drawable.ic_arrow);
        int width = decodeResource.getWidth();
        int height = decodeResource.getHeight();
        this.newWidth = getWidth();
        this.newHeight = getHeight();
        this.scaleWidth = ((float) this.newWidth) / ((float) width);
        this.scaleHeight = ((float) this.newHeight) / ((float) height);
        this.mMatrix.postScale(this.scaleWidth, this.scaleHeight);
        this.indicatorBitmap = Bitmap.createBitmap(decodeResource, 0, 0, width, height,
                this.mMatrix, true);
    }

    public void refresh() {
        postInvalidateDelayed(ANIMATION_INTERVAL);
    }

    public void onMeasure(int i, int i2) {
        super.onMeasure(i, i2);
    }

    public void onDraw(Canvas canvas) {
        super.onDraw(canvas);
        if (this.indicatorBitmap == null) {
            init();
        }
        float f = World.dbCount;

        if (f <= 0.0f) {
            f = 0.0f;
        }

        if (SoundRecorderJava.runMeter)
            this.mMatrix.setRotate(getAngle(f), newWidth / 2, newHeight);
        else
            this.mMatrix.setRotate(getAngle(0.0f), newWidth / 2, newHeight);

        canvas.drawBitmap(this.indicatorBitmap, this.mMatrix, this.paint);
    }
}
