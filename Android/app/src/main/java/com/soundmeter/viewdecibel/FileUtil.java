package com.soundmeter.viewdecibel;

import android.content.Context;
import android.os.Environment;

import java.io.File;
import java.io.IOException;

/**
 * Created by bodekjan on 2016/8/8.
 */
public class FileUtil {
    private static final String TAG = "FileUtil";

    public static final String LOCAL = "SoundMeter1";

    //public static final String LOCAL_PATH = Environment.getExternalStorageDirectory().getPath() + File.separator;

    //public static final String REC_PATH = LOCAL_PATH + LOCAL + File.separator;

//    public static String REC_PATH;
//
//    static {
//        REC_PATH = getAppPublicStorageDir();
//
//    }
//
//    public static String getAppPublicStorageDir() {
//        String path;
//
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) { // tested on Android 11
//            path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOCUMENTS).getPath();
//        } else {
//            path = Environment.getExternalStorageDirectory().getPath();
//        }
//        //String path = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).getPath();
//
//        File file = new File(path + "/" + LOCAL);
//        if (!file.exists()) {
//            boolean mkdir = file.mkdir();
//            if (!mkdir) return null;
//        }
//        return file.getPath();
//    }

    private FileUtil() {
    }
    public static boolean isExitSDCard() {
        return Environment.getExternalStorageState().equals(
                Environment.MEDIA_MOUNTED);
    }

//    private static boolean hasFile(String fileName) {
//        File f = createFile(fileName);
//        return null != f && f.exists();
//    }

    public static File createFile(String fileName, Context ctx) {

        File myCaptureFile = new File(ctx.getFilesDir(), fileName);
        if (myCaptureFile.exists()) {
            myCaptureFile.delete();
        }
        try {
            myCaptureFile.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return myCaptureFile;
    }


}
