package com.soundmeter.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.soundmeter.R
import com.soundmeter.datamodels.TimeAndSound
import java.util.*

class TimeSoundListAdapter(
    private val items: ArrayList<TimeAndSound>?,
    val context: Context
) :
    RecyclerView.Adapter<TimeSoundListAdapter.MyViewHolder>() {

    val TAG: String = "TimeSoundListAdapter"

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MyViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.row_time_sound, parent, false)
        return MyViewHolder(view)
    }

    class MyViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val tvTime: TextView = itemView.findViewById(R.id.tv_time)
        val tvSound: TextView = itemView.findViewById(R.id.tv_sound)
    }

    override fun getItemCount(): Int {
        return items!!.size
    }

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: MyViewHolder, position: Int) {
        val dataModel: TimeAndSound = items!![position]
        holder.tvTime.text = dataModel.time
        holder.tvSound.text = dataModel.soundDBA
    }
}