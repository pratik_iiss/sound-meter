package com.soundmeter.datamodels

class TimeAndSound {
    var time: String = ""
    var soundDBA: String = ""

    constructor(time: String, soundDBA: String) {
        this.time = time
        this.soundDBA = soundDBA
    }

    override fun toString(): String {
        return "TimeAndSound(time='$time', soundDBA='$soundDBA')"
    }
}