package com.soundmeter.activities

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.pm.PackageManager
import android.media.*
import android.os.Bundle
import android.os.Handler
import android.text.Html
import android.util.Log
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.soundmeter.R
import com.soundmeter.adapter.TimeSoundListAdapter
import com.soundmeter.constants.CommonContants
import com.soundmeter.datamodels.TimeAndSound
import com.soundmeter.viewdecibel.FileUtil
import com.soundmeter.viewdecibel.World
import org.jtransforms.fft.DoubleFFT_1D
import java.lang.Math.log10
import java.text.DecimalFormat
import java.util.*
import kotlin.math.abs
import kotlin.math.pow
import kotlin.math.sqrt

class MainActivity : AppCompatActivity() {

    val TAG = "MainActivity"
    lateinit var recorder: AudioRecord
    var bufferSize: Int = 0
    var recordedTimeAndSound: ArrayList<TimeAndSound> = ArrayList()
    var recordedSound: ArrayList<Double> = ArrayList()
    lateinit var btnStart: Button
    lateinit var tvStatus: TextView
    lateinit var recyclerTimeAndSound: RecyclerView
    lateinit var llListData: LinearLayout
    lateinit var dataAdapter: TimeSoundListAdapter
    var stop = false
    lateinit var data: ShortArray

    lateinit var mRecorder: com.soundmeter.viewdecibel.MyAudioRecorder
    private val iCoundSpeeddB = 0
    var count = 0
    val timer = Timer()
    var timer1 = Timer()
    var time = 0.2

    private val permissionArray = arrayOf(
        Manifest.permission.RECORD_AUDIO
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        init()
    }

    private fun init() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.findViewById<ImageView>(R.id.img_back).setOnClickListener { onBackPressed() }
        tvStatus = findViewById(R.id.tv_status)
        findViewById<Button>(R.id.btn_demo).setOnClickListener {
            startActivity(
                Intent(
                    this,
//                    LiveSoundTrackActivity::class.java
                    SoundRecorderJava::class.java
                )
            )
        }
        llListData = findViewById(R.id.ll_list_data)
        recyclerTimeAndSound = findViewById(R.id.recycler_data)
        recyclerTimeAndSound.layoutManager = LinearLayoutManager(
            this,
            RecyclerView.VERTICAL, false
        )
        dataAdapter = TimeSoundListAdapter(recordedTimeAndSound, this)
        recyclerTimeAndSound.adapter = dataAdapter

        tvStatus.text = "Click Start to measure sound!!"
        btnStart = findViewById(R.id.btn_start)
        btnStart.setOnClickListener {
            checkPermissionAndStartRecording()
        }

        checkPermission()
    }

    /*override fun onResume() {
        super.onResume()

        bufferSize = AudioRecord.getMinBufferSize(
            44100,
            AudioFormat.CHANNEL_IN_MONO,
            AudioFormat.ENCODING_PCM_16BIT
        )

//            bufferSize *= 4
        Log.e(TAG, "bufferSize: $bufferSize")
        data = ShortArray(bufferSize)
        recorder = AudioRecord(
            MediaRecorder.AudioSource.VOICE_RECOGNITION,
            44100, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, bufferSize
        )
    }*/

    private fun checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
            != PackageManager.PERMISSION_GRANTED
        ) {
            Log.e(TAG, "request permission...")
            ActivityCompat.requestPermissions(this, permissionArray, 1)
        } else {
            Log.e(TAG, "permission granted...")

            mRecorder = com.soundmeter.viewdecibel.MyAudioRecorder()
            val file = FileUtil.createFile("temp.amr", this)
            mRecorder.setMyRecAudioFile(file)
            mRecorder.startRecorder()

            RecordTask(this, 95)
        }
    }

    private fun checkPermissionAndStartRecording() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
            != PackageManager.PERMISSION_GRANTED
        ) {
            Log.e(TAG, "request permission...")
            ActivityCompat.requestPermissions(this, permissionArray, 1)
        } else {
            Log.e(TAG, "permission granted...")

            btnStart.isEnabled = false
            tvStatus.text = "Measuring sound..."

            startRecording()
        }
    }

    private fun startRecording() {
        count = 0
        stop = false
        llListData.visibility = View.GONE
        recordedSound.clear()
        recordedTimeAndSound.clear()

        try {
            /*bufferSize = AudioRecord.getMinBufferSize(
                44100,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT
            )
            data = ShortArray(bufferSize)
            recorder = AudioRecord(
                MediaRecorder.AudioSource.VOICE_RECOGNITION,
                44100, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, bufferSize
            )
            recorder.startRecording()*/

//            World.minDB = 200.0f
//            World.lastDbCount = 0.0f
//            World.maxDB = 0.0f

//            val file = FileUtil.createFile("temp.amr", this)
//            mRecorder.setMyRecAudioFile(file)
//            mRecorder.startRecorder()

            if (mRecorder.startRecorder()) {
                timer1 = Timer()
                timer1.scheduleAtFixedRate(object : TimerTask() {
                    override fun run() {
                        runOnUiThread {
                            if (!stop) {
//                            getNoiseLevel(count)
                                dBALoop(count)
                            }
                        }
                    }
                }, 0, 200)
            }
        } catch (e: Exception) {
            Log.e(TAG, "initializeAndStartRecord: $e")

            Toast.makeText(this, "Problem: $e", Toast.LENGTH_SHORT).show()

            btnStart.isEnabled = true
            tvStatus.text = "Click Start to Measure again !!"
        }
    }

    private fun callHandler() {
        val handler = Handler()
        handler.postDelayed({
            stop = true
            btnStart.isEnabled = true
            tvStatus.text = "Click Start to Measure again !!"

            timer1.cancel()

//            World.minDB = 200.0f
//            World.dbCount = 0.0f
//            World.lastDbCount = 0.0f
//            World.maxDB = 0.0f

            showRecordedData()
        }, 0)
    }

    private fun getNoiseLevel(entryCount: Int) {
        val formater = DecimalFormat("0.0")
        var count = 0

        //recording data
        recorder.read(data, 0, bufferSize)

        var average = 0.0
        for (s in data) {
            if (s > 0) {
                average += abs(s.toInt()).toDouble()
                count++
            }
        }
        /*val x = average / bufferSize
        if (x != 0.0) {
            val pressure = x / 51805.5336
            val REFERENCE = 0.00002
            val avgDB = (20 * log10(pressure / REFERENCE))
            Log.e(TAG, "dBA: " + (formater.format(avgDB)))
        }*/

//        val db = (20 * log10(average)) + 8.25
//        val db = (10 * log10(average))
//        val db = (10 * log10(average / count)) + 8.25
//        val db = (20 * log10(average / count)) + 8.25
//        val db = (10 * log10(average / count))
        val db = (10 * log10(average)) + 8.25
        try {
            val dBA: Double
            val frequency = getFFT()
            if (frequency >= 22 && frequency < 44) {
                dBA = db + CommonContants.adjustment22_44
//            dBA += CommonContants.adjustment22_44
            } else if (frequency >= 44 && frequency < 88)
                dBA = db + CommonContants.adjustment44_88
            else if (frequency >= 88 && frequency < 177)
                dBA = db + CommonContants.adjustment88_177
            else if (frequency >= 177 && frequency < 355)
                dBA = db + CommonContants.adjustment177_355
            else if (frequency >= 355 && frequency < 710)
                dBA = db + CommonContants.adjustment355_710
            else if (frequency >= 710 && frequency < 1420)
                dBA = db + CommonContants.adjustment710_1420
            else if (frequency >= 1420 && frequency < 2840)
                dBA = db + CommonContants.adjustment1420_2840
            else if (frequency >= 2840 && frequency < 5680)
                dBA = db + CommonContants.adjustment2840_5680
            else if (frequency >= 5680 && frequency < 11360)
                dBA = db + CommonContants.adjustment5680_11360
            else if (frequency >= 11360 && frequency < 22720)
                dBA = db + CommonContants.adjustment11360_22720
            else
                dBA = db

            Log.e(
                TAG,
                "db: " + (formater.format(db)) + "  dBA: " + (formater.format(dBA)) + "  Frequency: $frequency"
            )
        } catch (e: Exception) {
            Log.e(TAG, "dBA: $e")
        }

        if (recordedSound.size < 4)
            recordedSound.add(db)

        Log.e(TAG, "size: " + recordedSound.size)
//        if (recordedSound.size > 0) {
//            llListData.visibility = View.VISIBLE
//            when (entryCount) {
//                0 -> recordedTimeAndSound.add(TimeAndSound("0.5", recordedSound[0]))
//                1 -> recordedTimeAndSound.add(TimeAndSound("1.0", recordedSound[1]))
//                2 -> recordedTimeAndSound.add(TimeAndSound("1.5", recordedSound[2]))
//                3 -> recordedTimeAndSound.add(TimeAndSound("2.0", recordedSound[3]))
//            }
//            dataAdapter.notifyDataSetChanged()
//        }
    }

    private fun getFFT(): Double {
        val fft = DoubleArray(2 * data.size)
        var avg = 0.0
        var amplitude = 0.0

        for (i in data.indices) {
            fft[i] = data[i] / Short.MAX_VALUE.toDouble()
        }

        // fft
        val transform = DoubleFFT_1D(bufferSize.toLong())
        transform.realForwardFull(fft)

        // calculate the sum of amplitudes
        var i = 0
        while (i < fft.size) {
            amplitude += sqrt(fft[i].pow(2.0) + fft[i + 1].pow(2.0))
            avg += amplitude * CommonContants.A_WEIGHT_COEFFICIENTS[i / 2]
            i += 2
        }

        return avg / data.size
    }

    private fun dBALoop(entryCount: Int) {
        try {
            /*val decimalFormat = DecimalFormat("####0.0")
            if (recordedSound.size < 4) {
                recordedSound.add(World.dbCount.toDouble())

                if (recordedSound.size > 0) {
                    llListData.visibility = View.VISIBLE
                    recordedTimeAndSound.add(
                        TimeAndSound(
                            time.toString(),
                            decimalFormat.format(recordedSound[entryCount])
                        )
                    )
                    time += 0.5
                    dataAdapter.notifyDataSetChanged()
                }
            }*/

            val maxAmp = mRecorder.maxAmplitude
            if (maxAmp > 0.0f && maxAmp < 1000000.0f) {
                val decimalFormat = DecimalFormat("####0.0")
                val log10 = log10(maxAmp.toDouble()).toFloat() * 20.0f
                World.setDbCount(iCoundSpeeddB.toFloat() + log10, log10)
                if (World.dbCount < 0.0f) {
                    World.dbCount = 0.0f
                }
                Log.e(TAG, World.dbCount.toString() + " dB")
                Log.e(TAG, "count: $entryCount")
                Log.e(TAG, "time: $time")

//                if (recordedSound.size < 4) {
                recordedSound.add(World.dbCount.toDouble())

                if (recordedSound.size > 0) {
                    llListData.visibility = View.VISIBLE
                    recordedTimeAndSound.add(
                        TimeAndSound(
                            String.format("%.1f", time),
                            decimalFormat.format(recordedSound[entryCount])
                        )
                    )
                    time += 0.2
                    count += 1
                    /*when (entryCount) {
                        0 -> recordedTimeAndSound.add(TimeAndSound("0.5", recordedSound[0]))
                        1 -> recordedTimeAndSound.add(TimeAndSound("1.0", recordedSound[1]))
                        2 -> recordedTimeAndSound.add(TimeAndSound("1.5", recordedSound[2]))
                        3 -> recordedTimeAndSound.add(TimeAndSound("2.0", recordedSound[3]))
                    }*/
                    dataAdapter.notifyDataSetChanged()
                }
//                }

                if (String.format("%.1f", time) == "2.2")
                    callHandler()
            }
        } catch (e: java.lang.Exception) {
            Log.e(TAG, "dBALoop: $e")
        }
    }

    private fun showRecordedData() {
//        recorder.stop()
//        recorder.release()
//        mRecorder.stopRecording()
        time = 0.2
        Log.e(TAG, "sound list: " + recordedSound.size)
        if (recordedSound.size > 0 &&
            (recordedTimeAndSound.size == recordedSound.size)
        ) {
            btnStart.isEnabled = true
            val maxValue = Collections.max(recordedSound)
            val index = recordedSound.indexOf(maxValue)
            val textTime = "<b>" + recordedTimeAndSound[index].time + " second</b>"
            val textSound = "<b>" + recordedTimeAndSound[index].soundDBA + " dB</b>"
            val text = "Click Start to Measure !!"
            tvStatus.text = "Data recorded: \n\n" + Html.fromHtml(textTime) + "\n" +
                    Html.fromHtml(textSound) + "\n\n" + text

            if (recordedTimeAndSound.size > 0) {
                llListData.visibility = View.VISIBLE
            } else
                llListData.visibility = View.GONE
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1) {
            for (permission in permissions) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        this@MainActivity,
                        permission!!
                    )
                ) {
                    Log.e("Permission", "Deny")
                } else {
                    if (ActivityCompat.checkSelfPermission(
                            this@MainActivity,
                            permission
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        Log.e("Permission", "Allowed")
                        checkPermission()
                    } else {
                        Log.e("Permission", "Never ask again")

                        break
                    }
                }
            }
        }
    }

    inner class RecordTask(var context: Activity, i: Int) : TimerTask() {
        var timer = Timer()

        internal inner class PointMeter : Runnable {
            override fun run() {
                try {
                    onGoingLoop()
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        }

        override fun run() {
            val activity = context
            if (activity != null) {
                if (!activity.isFinishing) {
                    context.runOnUiThread(PointMeter())
                    return
                }
            }
            cancel()
        }

        init {
            timer.scheduleAtFixedRate(this, 0, i.toLong())
        }
    }

    fun onGoingLoop() {
        try {
            val volume = mRecorder.maxAmplitude
            if (volume > 0.0f && volume < 1000000.0f) {
                val log10 = log10(volume.toDouble()).toFloat() * 20.0f
                World.setDbCount(iCoundSpeeddB.toFloat() + log10, log10)
                if (World.dbCount < 0.0f) {
                    World.dbCount = 0.0f
                }
            }
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
    }
}