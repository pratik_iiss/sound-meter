package com.soundmeter.activities

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.media.*
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.soundmeter.R
import com.soundmeter.constants.CommonContants
import org.jtransforms.fft.DoubleFFT_1D
import java.lang.Math.log10
import java.text.DecimalFormat
import java.util.*
import kotlin.math.abs
import kotlin.math.pow
import kotlin.math.sqrt

class LiveSoundTrackActivity : AppCompatActivity() {

    val TAG = "LiveSoundTrackActivity"
    lateinit var recorder: AudioRecord
    lateinit var data: ShortArray
    var bufferSize: Int = 0
    lateinit var btnStart: Button
    lateinit var btnStop: Button
    lateinit var tvStatus: TextView
    lateinit var timer: Timer

    private val permissionArray = arrayOf(
        Manifest.permission.RECORD_AUDIO
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_live_sound_track)

        init()
    }

    private fun init() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.findViewById<ImageView>(R.id.img_back).setOnClickListener { onBackPressed() }
        tvStatus = findViewById(R.id.tv_status)
        tvStatus.text = "Click Start to measure sound!!"
        btnStart = findViewById(R.id.btn_start)
        btnStop = findViewById(R.id.btn_stop)
        btnStart.setOnClickListener {
            checkPermissionAndStartRecording()
        }
        btnStop.setOnClickListener {
            btnStart.isEnabled = true
            btnStop.isEnabled = false
            tvStatus.text = "Click Start to Measure again !!"
            timer.cancel()
//            recorder.stop()
//            recorder.release()
        }
        btnStart.isEnabled = true
        btnStop.isEnabled = false

        checkPermission()


//        checkPermissionAndStartRecording()
    }

    override fun onResume() {
        super.onResume()

        bufferSize = AudioRecord.getMinBufferSize(
            44100,
            AudioFormat.CHANNEL_IN_MONO,
            AudioFormat.ENCODING_PCM_16BIT
        )
//        bufferSize = 2048

        bufferSize *= 2
        Log.e(TAG, "bufferSize: $bufferSize")
//        data = ShortArray(bufferSize)
        data = ShortArray(bufferSize)
        recorder = AudioRecord(
            MediaRecorder.AudioSource.MIC,
            44100, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, bufferSize
        )
    }

    private fun checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
            != PackageManager.PERMISSION_GRANTED
        ) {
            Log.e(TAG, "request permission...")
            ActivityCompat.requestPermissions(this, permissionArray, 1)
        } else {
            Log.e(TAG, "permission granted...")
        }
    }

    private fun checkPermissionAndStartRecording() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
            != PackageManager.PERMISSION_GRANTED
        ) {
            Log.e(TAG, "request permission...")
            ActivityCompat.requestPermissions(this, permissionArray, 1)
        } else {
            Log.e(TAG, "permission granted...")

            btnStart.isEnabled = false
            tvStatus.text = "Measuring sound..."

            startRecording()
        }
    }

    private fun startRecording() {
        try {
            bufferSize = AudioRecord.getMinBufferSize(
                44100,
                AudioFormat.CHANNEL_IN_MONO,
                AudioFormat.ENCODING_PCM_16BIT
            )
//            bufferSize = 2048
            bufferSize *= 2
            recorder = AudioRecord(
                MediaRecorder.AudioSource.MIC,
                44100, AudioFormat.CHANNEL_IN_MONO, AudioFormat.ENCODING_PCM_16BIT, bufferSize
            )
//            data = ShortArray(bufferSize)
            data = ShortArray(bufferSize)
            Log.e(TAG, "data: " + data.size)
            recorder.startRecording()
            btnStart.isEnabled = false
            btnStop.isEnabled = true
            timer = Timer()
            timer.scheduleAtFixedRate(object : TimerTask() {
                override fun run() {
                    runOnUiThread {
                        getNoiseLevel()
                    }
                }
            }, 0, 200)
        } catch (e: Exception) {
            Log.e(TAG, "initializeAndStartRecord: $e")

            Toast.makeText(this, "Problem: $e", Toast.LENGTH_SHORT).show()

            btnStart.isEnabled = true
            tvStatus.text = "Click Start to Measure again !!"
        }
    }

    @SuppressLint("SetTextI18n")
    private fun getNoiseLevel() {
        val formater = DecimalFormat("0.0")
        var count = 0

        recorder.read(data, 0, bufferSize)

        var average = 0.0
        for (s in data) {
            if (s > 0) {
                average += abs(s.toInt()).toDouble()
                count++
            }
        }
        /*for (s in (0 until data.size - 1)) {
            average += abs(data[s].toInt()).toDouble()
        }*/
//        val amplitude = average / (data.size)
//        val dbAm = (20 * log10(amplitude))
//        val dbAvg = (10 * log10(average)) + 8.25

        /*val x = average / bufferSize
        if (x != 0.0) {
            val pressure = x / 51805.5336
            val REFERENCE = 0.00002
            val avgDB = (20 * log10(pressure / REFERENCE))
            Log.e(TAG, "dBA: " + (formater.format(avgDB)))
        }*/

//        val db = (20 * log10(average)) + 8.25
//        val db = (20 * log10(average / count)) + 8.25
        var db = (10 * log10(average)) + 8.25
//        var db = (10 * log10(average))
//        val db = (10 * log10(average / count)) + 8.25
//        val db = (10 * log10(average / count))

        val dBA: Double
//        val frequency = getFFT()
        val frequency = getFrequency()
        if (frequency >= 22 && frequency < 44) {
            dBA = db + CommonContants.adjustment22_44
//            dBA += CommonContants.adjustment22_44
        } else if (frequency >= 44 && frequency < 88)
            dBA = db + CommonContants.adjustment44_88
        else if (frequency >= 88 && frequency < 177)
            dBA = db + CommonContants.adjustment88_177
        else if (frequency >= 177 && frequency < 355)
            dBA = db + CommonContants.adjustment177_355
        else if (frequency >= 355 && frequency < 710)
            dBA = db + CommonContants.adjustment355_710
        else if (frequency >= 710 && frequency < 1420)
            dBA = db + CommonContants.adjustment710_1420
        else if (frequency >= 1420 && frequency < 2840)
            dBA = db + CommonContants.adjustment1420_2840
        else if (frequency >= 2840 && frequency < 5680)
            dBA = db + CommonContants.adjustment2840_5680
        else if (frequency >= 5680 && frequency < 11360)
            dBA = db + CommonContants.adjustment5680_11360
        else if (frequency >= 11360 && frequency < 22720)
            dBA = db + CommonContants.adjustment11360_22720
        else
            dBA = db

//        Log.e(
//            TAG,
//            "db: " + (formater.format(db)) + "  dBA: " + (formater.format(dBA)) + "  Frequency: $frequency"
//        )
//        tvStatus.text =
//            formater.format(db) + " dBA\n\nFrequency: " + (formater.format(frequency)) + " Hz"

        Log.e(TAG, "db: " + (formater.format(db)) + "  Frequency: $frequency")

        val avg10 = db / 10
        if (db > 80 && frequency < 3500)
            db += avg10
        if (db > 80 && frequency >= 3500)
            db -= avg10

        tvStatus.text = formater.format(db) + " dBA\n" + "Frequency: $frequency"
    }

    private fun getFFT(): Double {
        val fft = DoubleArray(2 * data.size)
        var avg = 0.0
        var amplitude = 0.0

        for (i in data.indices) {
            fft[i] = data[i] / Short.MAX_VALUE.toDouble()
        }

        // fft
        val transform = DoubleFFT_1D(bufferSize.toLong())
        transform.realForwardFull(fft)

        // calculate the sum of amplitudes
        var i = 0
        while (i < fft.size) {
            amplitude += sqrt(fft[i].pow(2.0) + fft[i + 1].pow(2.0))
            avg += amplitude * CommonContants.A_WEIGHT_COEFFICIENTS[i / 2]
            i += 2
        }

        return avg / data.size
    }

    private fun getFrequency(): Double {
        val sampleRate = 44100
        val numSamples = data.size
        var numCrossing = 0
        for (p in 0 until numSamples - 1) {
            if (data[p] > 0 && data[p + 1] <= 0 ||
                data[p] < 0 && data[p + 1] >= 0
            ) {
                numCrossing++
            }
        }
        val numSecondsRecorded = numSamples.toFloat() / sampleRate.toFloat()
        val numCycles = (numCrossing / 2).toFloat()
        val frequency = numCycles / numSecondsRecorded
        return frequency.toDouble()
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1) {
            for (permission in permissions) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        this@LiveSoundTrackActivity,
                        permission!!
                    )
                ) {
                    Log.e("Permission", "Deny")
                } else {
                    if (ActivityCompat.checkSelfPermission(
                            this@LiveSoundTrackActivity,
                            permission
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        Log.e("Permission", "Allowed")
                        checkPermission()
                    } else {
                        Log.e("Permission", "Never ask again")

                        break
                    }
                }
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()

        if (!btnStart.isEnabled)
            timer.cancel()
        recorder.stop()
        recorder.release()
    }
}
