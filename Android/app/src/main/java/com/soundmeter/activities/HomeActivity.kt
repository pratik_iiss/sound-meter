package com.soundmeter.activities

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.soundmeter.R

class HomeActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)


        findViewById<Button>(R.id.btn_mock_app).setOnClickListener {
            startActivity(
                Intent(
                    this,
                    MainActivity::class.java
                )
            )
        }

        findViewById<Button>(R.id.btn_demo).setOnClickListener {
            startActivity(
                Intent(
                    this,
                    LiveSoundTrackActivity::class.java
                )
            )
        }
    }
}