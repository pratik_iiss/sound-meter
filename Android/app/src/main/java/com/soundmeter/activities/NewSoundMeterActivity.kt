package com.soundmeter.activities

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.pm.PackageManager
import android.media.*
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.github.mikephil.charting.charts.LineChart
import com.marcinmoskala.arcseekbar.ArcSeekBar
import com.soundmeter.R
import com.soundmeter.customClass.MyAudioRecorder
import com.soundmeter.customClass.MyDataSqlite
import com.soundmeter.customClass.SpeedoMeter
import com.soundmeter.customClass.World
import java.io.File
import java.io.IOException
import java.util.*

class NewSoundMeterActivity : AppCompatActivity() {

    val TAG = "NewSoundMeterActivity"
    lateinit var btnStart: Button
    lateinit var btnStop: Button
    lateinit var tvStatus: TextView
    lateinit var tvMinRun: TextView
    lateinit var tvAvgRun: TextView
    lateinit var tvMaxRun: TextView
    lateinit var timer: Timer
    private val mSeekBar: ArcSeekBar? = null
    private val mMyDataSqlite: MyDataSqlite? = null
    var refreshed = false
    var speedometer: SpeedoMeter? = null
    private var bListener = true
    private val volume = 10000f
    private var isPause = false
    private var mRecorder: MyAudioRecorder? = null
    private var isRun = false
    private var initGap = 0.0f
    private var pauseMinDb = -999.0f
    var chk_going = false
    val mChart: LineChart? = null
    var recordTask: RecordTask? = null

    private val permissionArray = arrayOf(
        Manifest.permission.RECORD_AUDIO
    )

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_new_sound_meter)

        init()
    }

    private fun init() {
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        toolbar.findViewById<ImageView>(R.id.img_back).setOnClickListener { onBackPressed() }
        tvStatus = findViewById(R.id.tv_status)
        tvMinRun = findViewById(R.id.txtMinRun)
        tvAvgRun = findViewById(R.id.txtAVGRun)
        tvMaxRun = findViewById(R.id.txtMaxRun)
        tvStatus.text = "0"
        btnStart = findViewById(R.id.btn_start)
        btnStop = findViewById(R.id.btn_stop)
        mRecorder = MyAudioRecorder

        btnStart.setOnClickListener {
            checkPermissionAndStartRecording()
        }

        btnStop.setOnClickListener {
            btnStart.isEnabled = true
            btnStop.isEnabled = false
            tvStatus.text = "0"
            timer.cancel()
//            recorder.stop()
//            recorder.release()

            initGap = World.dbCount / 10.0f
            isRun = false
            bListener = false
            mRecorder?.stopRecording()
            chk_going = false
            pauseMinDb = -999.0f
            World.minDB = 200.0f
            World.lastDbCount = 0.0f
            World.maxDB = 0.0f

        }
        btnStart.isEnabled = true
        btnStop.isEnabled = false

        checkPermission()
    }

    override fun onResume() {
        super.onResume()


        onStartMeter()
    }

    private fun onStartMeter() {
        val file = File(filesDir, "temp.amr")
        if (file.exists()) {
            file.delete()
        }
        try {
            file.createNewFile()
        } catch (e: IOException) {
            e.printStackTrace()
        }

        if (file != null) {
            isPause = false
            isRun = true
            chk_going = false
            pauseMinDb = World.minDB
            startRecord(file)
        }
        bListener = true
    }

    private fun startRecord(fFile: File) {
        try {
            mRecorder!!.setMyRecAudioFile(fFile)
            if (mRecorder!!.startRecorder()) {
                startListenAudio()
            }
        } catch (e: java.lang.Exception) {
            Log.e(TAG, "startRecord: $e")
        }
    }

    private fun startListenAudio() {
        if (recordTask != null) {
            recordTask!!.cancel()
        }
        recordTask = RecordTask(this, 200)
    }

    private fun checkPermission() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
            != PackageManager.PERMISSION_GRANTED
        ) {
            Log.e(TAG, "request permission...")
            ActivityCompat.requestPermissions(this, permissionArray, 1)
        } else {
            Log.e(TAG, "permission granted...")
        }
    }

    private fun checkPermissionAndStartRecording() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO)
            != PackageManager.PERMISSION_GRANTED
        ) {
            Log.e(TAG, "request permission...")
            ActivityCompat.requestPermissions(this, permissionArray, 1)
        } else {
            Log.e(TAG, "permission granted...")

            btnStart.isEnabled = false
            tvStatus.text = "0"

            mRecorder?.startRecorder()

            onStartMeter()
        }
    }

    private fun startRecording() {
        try {
            pauseMinDb = World.minDB
            this.mRecorder?.startRecorder()
//            timer = Timer()
//            timer.scheduleAtFixedRate(object : TimerTask() {
//                override fun run() {
//                    runOnUiThread {
//                        getNoiseLevel()
//                    }
//                }
//            }, 0, 200)

        } catch (e: Exception) {
            Log.e(TAG, "initializeAndStartRecord: $e")

            Toast.makeText(this, "Problem: $e", Toast.LENGTH_SHORT).show()

            btnStart.isEnabled = true
            tvStatus.text = "-"
        }
    }

    @SuppressLint("SetTextI18n")
    private fun getNoiseLevel() {
        tvStatus.text = World.dbCount.toString()

        if (World.dbCount > 0.0f) {
            World.dbCount -= initGap
            initGap = World.dbCount / 10.0f
            if (initGap < 1.0f) {
                initGap = 1.0f
            }
            if (World.dbCount <= 0.0f) {
                tvMinRun.text = "-"
                tvAvgRun.text = "-"
                tvMaxRun.text = "-"
                mSeekBar?.progress = 0
                World.dbCount = 0.0f
            }
            speedometer?.refresh()
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String?>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 1) {
            for (permission in permissions) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(
                        this@NewSoundMeterActivity,
                        permission!!
                    )
                ) {
                    Log.e("Permission", "Deny")
                } else {
                    if (ActivityCompat.checkSelfPermission(
                            this@NewSoundMeterActivity,
                            permission
                        ) == PackageManager.PERMISSION_GRANTED
                    ) {
                        Log.e("Permission", "Allowed")
                        checkPermission()
                    } else {
                        Log.e("Permission", "Never ask again")

                        break
                    }
                }
            }
        }
    }

    override fun onBackPressed() {
        super.onBackPressed()

        if (!btnStart.isEnabled)
            timer.cancel()
    }


    inner class RecordTask(var context: Activity, i: Int) : TimerTask() {
        var timer = Timer()

        internal inner class PointMeter : Runnable {
            override fun run() {
                try {
                    if (World.dbCount > 0.0f) {
                        World.dbCount -= initGap
                        initGap = World.dbCount / 10.0f
                        if (initGap < 1.0f) {
                            initGap = 1.0f
                        }
                        if (World.dbCount <= 0.0f) {
                            tvMinRun.text = "-"
                            tvMaxRun.text = "-"
                            tvStatus.text = "-"
                            tvAvgRun.text = "-"

                            mSeekBar?.progress = 0
                            World.dbCount = 0.0f

                        }
                        speedometer?.refresh()
                    }
                } catch (e: java.lang.Exception) {
                    e.printStackTrace()
                }
            }
        }

        override fun run() {
            val activity = context
            if (activity != null) {
                if (!activity.isFinishing) {
                    if (!isPause) {
                        context.runOnUiThread(PointMeter())
                        return
                    }
                    return
                }
            }
            cancel()
        }

        init {
            timer.scheduleAtFixedRate(this, 0, i.toLong())
        }
    }
}
