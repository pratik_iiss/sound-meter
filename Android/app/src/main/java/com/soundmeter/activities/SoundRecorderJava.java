package com.soundmeter.activities;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;

import com.github.mikephil.charting.components.YAxis;
import com.marcinmoskala.arcseekbar.ArcSeekBar;
import com.soundmeter.R;
import com.soundmeter.viewdecibel.FileUtil;
import com.soundmeter.viewdecibel.MyAudioRecorder;
import com.soundmeter.viewdecibel.MyDataSqlite;
import com.soundmeter.viewdecibel.Speedometer;
import com.soundmeter.viewdecibel.World;

import java.io.File;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

public class SoundRecorderJava extends AppCompatActivity implements View.OnClickListener {

    String TAG = "SoundRecorderJava";
    private ArcSeekBar mSeekBar;
    private MyDataSqlite mMyDataSqlite;
    private TextView txtdB, txtRundB, txtNotifi, txtMin, txtRunMin, txtAVG, txtRunAVG, txtMax,
            txtRunMax, txtTitleBar, txtRunMeter;
    Button btnStart, btnStop;
    private static final int MY_PERMISSION_REQUEST = 1;
    boolean refreshed = false;
    Speedometer speedometer;
    private boolean bListener = true;
    private float volume = 10000;
    private boolean isPause = false;
    private MyAudioRecorder mRecorder;
    private boolean isRun = false;
    private float initGap = 0.0f;
    private float pauseMinDb = -999.0f;
    boolean chk_going = false;
    private RecordTask recordTask;
    private boolean isKeepOn;
    private boolean isHide;
    private YAxis leftAxis;
    private int iSpeed;
    private final int iCoundSpeeddB = 0;
    public static boolean runMeter = true;

    public class RecordTask extends TimerTask {
        Activity context;
        Timer timer = new Timer();

        class PointMeter implements Runnable {
            PointMeter() {
            }

            @Override
            public void run() {
                try {
                    if (isRun) {
                        SoundRecorderJava.this.MainLoop();
                        txtRunMeter.setText((int) World.dbCount + "");
                        return;
                    }

                    if (World.dbCount > 0.0f) {
                        World.dbCount -= initGap;
                        initGap = World.dbCount / 10.0f;
                        if (initGap < 1.0f) {
                            initGap = 1.0f;
                        }
                        if (World.dbCount <= 0.0f) {
                            txtRunMin.setText("0");
                            txtRunMax.setText("0");
                            txtRundB.setText("0");
                            txtRunAVG.setText("0");
                            txtNotifi.setText("0");
                            World.dbCount = 0.0f;
                            mSeekBar.setProgress(0);
                        }
                        speedometer.refresh();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        public RecordTask(Activity activity, int i) {
            this.context = activity;
            this.timer.scheduleAtFixedRate(this, 0, i);
        }

        @Override
        public void run() {
            Activity activity = this.context;
            if (activity != null) {
                if (!activity.isFinishing()) {
                    if (!isPause) {
                        this.context.runOnUiThread(new PointMeter());
                        return;
                    }
                    return;
                }
            }
            cancel();
        }
    }

    public void MainLoop() {
        if (this.isRun) {
            try {
                if (this.bListener) {
                    this.volume = this.mRecorder.getMaxAmplitude();
                    if (this.volume > 0.0f && this.volume < 1000000.0f) {
                        DecimalFormat decimalFormat = new DecimalFormat("####0.0");
                        float log10 = ((float) Math.log10(this.volume)) * 20.0f;
                        World.setDbCount(((float) iCoundSpeeddB) + log10, log10);
                        this.speedometer.refresh();

                        if (!this.chk_going) {
                            if (this.pauseMinDb < 0.0f) {
                                World.minDB = World.maxDB;
                            } else {
                                World.minDB = this.pauseMinDb;
                            }
                            this.chk_going = true;
                        }

                        this.mSeekBar.setProgress((int) World.dbCount);
                        this.txtRunMin.setText(decimalFormat.format(World.minDB));
                        this.txtRunAVG.setText(decimalFormat.format((World.minDB + World.maxDB) / 2.0f));
                        this.txtRunMax.setText(decimalFormat.format(World.maxDB));
                        if (World.dbCount < 0.0f) {
                            World.dbCount = 0.0f;
                        }
                        Log.e(TAG, "log10: " + log10 + "   dBA: " + World.dbCount);
                        this.txtRundB.setText(decimalFormat.format(World.dbCount));
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
                this.bListener = false;
            }
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_sound_meter);

        init();
    }

    public void onStartMeter() {
        File file = FileUtil.createFile("temp1.amr", getApplicationContext());
        if (file != null) {
            isPause = false;
            isRun = true;
            chk_going = false;
            pauseMinDb = World.minDB;
            startRecord(file);
        }
        bListener = true;
    }

    private void init() {
        Toolbar toolbar = findViewById(R.id.toolbar);
        toolbar.findViewById(R.id.img_back).setOnClickListener(view -> onBackPressed());

        mSeekBar = findViewById(R.id.arcSeekbar);
        mSeekBar.setProgressColor(getResources().getColor(R.color.colorSeekbarDrak));
        mSeekBar.setProgressBackgroundColor(getResources().getColor(R.color.colorSeekbarDrak2));
        txtdB = findViewById(R.id.txtUnit);
        txtRundB = findViewById(R.id.tv_status);
        txtMin = findViewById(R.id.txtMin);
        txtMax = findViewById(R.id.txtMAX);
        txtAVG = findViewById(R.id.txtAVG);

        txtRunMin = findViewById(R.id.txtMinRun);
        txtRunAVG = findViewById(R.id.txtAVGRun);
        txtRunMax = findViewById(R.id.txtMaxRun);

        speedometer = findViewById(R.id.speedmeter);

        btnStart = findViewById(R.id.btn_start);
        btnStop = findViewById(R.id.btn_stop);
        btnStart.setOnClickListener(this);
        btnStop.setOnClickListener(this);
        mRecorder = new MyAudioRecorder();


        speedometer.setEnabled(true);
        btnStart.performClick();
    }

    public void startRecord(File fFile) {
        try {
            mRecorder.setMyRecAudioFile(fFile);
            if (mRecorder.startRecorder()) {
                startListenAudio();
            }
        } catch (Exception e) {

            e.printStackTrace();
        }
    }

    public void startListenAudio() {
        RecordTask recordTask = this.recordTask;
        if (recordTask != null) {
            recordTask.cancel();
        }
        this.recordTask = new RecordTask(this, 200);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btn_start:
                runMeter = true;
                speedometer.setEnabled(true);
                btnStart.setEnabled(false);
                btnStop.setEnabled(true);
                bListener = true;
                isRun = true;
                chk_going = true;
                pauseMinDb = World.minDB;
//                mRecorder.startRecorder();

                if (checkAndRequestPermissionIfNeeded()) {
                    onStartMeter();
                }
                break;

            case R.id.btn_stop:
                runMeter = false;
                btnStart.setEnabled(true);
                btnStop.setEnabled(false);
                initGap = World.dbCount / 10.0f;
                isRun = false;
                bListener = false;
                mRecorder.stopRecording();
                chk_going = false;
                pauseMinDb = -999.0f;
                World.minDB = 200.0f;
                World.lastDbCount = 0.0f;
                World.maxDB = 0.0f;
                speedometer.setEnabled(false);
                mSeekBar.setProgress(0);
                break;
            case R.id.img_reload:
                runMeter = true;
                isRun = true;
                speedometer.setEnabled(true);
                World.minDB = 200.0f;
                World.dbCount = 0.0f;
                World.lastDbCount = 0.0f;
                World.maxDB = 0.0f;
                speedometer.refresh();
                mSeekBar.setProgress(0);
                break;
        }
    }

    private boolean checkAndRequestPermissionIfNeeded() {

        boolean res = false;

        String[] params = null;
        String writeExternalStorage = Manifest.permission.WRITE_EXTERNAL_STORAGE;
        String readExternalStorage = Manifest.permission.READ_EXTERNAL_STORAGE;
        String audioPermission = Manifest.permission.RECORD_AUDIO;

        int hasWriteExternalStoragePermission;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            hasWriteExternalStoragePermission = PackageManager.PERMISSION_GRANTED;
        } else {
            hasWriteExternalStoragePermission = ActivityCompat.checkSelfPermission(this, writeExternalStorage);
        }

        int hasReadExternalStoragePermission = ActivityCompat.checkSelfPermission(this, readExternalStorage);
        int hasAudioPermission = ActivityCompat.checkSelfPermission(this, audioPermission);

        List<String> permissions = new ArrayList<>();

        if (hasWriteExternalStoragePermission != PackageManager.PERMISSION_GRANTED)
            permissions.add(writeExternalStorage);
        if (hasReadExternalStoragePermission != PackageManager.PERMISSION_GRANTED)
            permissions.add(readExternalStorage);
        if (hasAudioPermission != PackageManager.PERMISSION_GRANTED)
            permissions.add(audioPermission);

        if (!permissions.isEmpty()) {
            params = permissions.toArray(new String[permissions.size()]);
        }
        if (params != null && params.length > 0) {
            ActivityCompat.requestPermissions(SoundRecorderJava.this, params, MY_PERMISSION_REQUEST);
        } else {

            res = true;
        }

        return res;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions,
                                           @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_PERMISSION_REQUEST && grantResults != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            onStartMeter();

        }
    }

    @Override
    protected void onDestroy() {
        mRecorder.stopRecording();
        super.onDestroy();
    }
}
